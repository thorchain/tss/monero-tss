#!/bin/sh
while ! nc -z tss0 8080; do
  echo sleeping
  sleep 1
done

echo $PRIVKEY | /go/bin/tss -tss-port :8080 -peer /dns4/tss0/tcp/6668/ipfs/$(curl http://tss0:8080/p2pid) -p2p-port 6668 -loglevel debug
